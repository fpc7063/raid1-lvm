#!/bin/bash
set -ex

# Install necessary packages (just in case)
apt-get update
apt-get install -y gdisk util-linux dosfstools e2fsprogs lvm2 bash-completion debootstrap arch-install-scripts xfsprogs

source /tmp/chroot/usr/bin/vars.sh

# Destroy all old lvm2 config
set +e
sudo vgremove -ff os-vg
LVM_OLD_PVS=$(sudo pvs | tail -n+2 | awk '{print $1}')
set -e

for DISK in $DISKS; do
	echo "Destroying GPT data on disk: $DISK"
	# Destroy old partitions
	sudo wipefs --all $DISK
	# Add GPT table on disks
	sudo sgdisk -z $DISK
	# Add three partitions to each disk
	sudo sgdisk -n 1:0:+1G -t 1:ef00 -c 1:"EFI System" $DISK
	sudo mkfs.vfat -F32 "$DISK"1
	sudo sgdisk -n 2:0:5G -t 2:fd00 -c 2:"/boot" $DISK
	sudo mkfs.ext4 -F "$DISK"2
	sudo sgdisk -n 3:0:0 -t 3:8e00 -c 3:"lvm2 pv" $DISK
	sudo pvcreate -f -M 2 ${DISK}3
	LVM_PVS="$LVM_PVS ${DISK}3"
done

sudo vgcreate "$VG_NAME" $LVM_PVS
sudo lvcreate --yes --type raid1 --raidintegrity y -L "$OS_LV_SIZE" -n "$LV_NAME" "$VG_NAME"

# loop till syncronization is 100%
while [[ $(sudo lvs | tail -n+2 | awk '{printf $5}') != '100.00' ]]; do
	echo "LVM raid0 SYNCRONIZATION at: $(sudo lvs | tail -n+2 | awk '{printf $5}')%"
	sleep 10
done


mkdir -p "$CHROOT_PATH"
mkfs.xfs "/dev/$VG_NAME/$LV_NAME"
sudo mount "/dev/$VG_NAME/$LV_NAME" "$CHROOT_PATH"
sudo mkdir "$CHROOT_PATH/boot"
sudo mount /dev/vda2 "$CHROOT_PATH/boot"
sudo mkdir "$CHROOT_PATH/boot/efi"
sudo mount /dev/vda1 "$CHROOT_PATH/boot/efi"

sudo debootstrap bookworm "$CHROOT_PATH" http://deb.debian.org/debian > /dev/null

# Setup scripts on chroot
sudo rsync -avhu --progress /tmp/chroot/ "$CHROOT_PATH"
# Run inside chroot
sudo arch-chroot "$CHROOT_PATH" "/usr/bin/inside-chroot.sh"

# Save efi data on both disks
#efibootmgr -c -d /dev/vda -p 1 -L "Debian1" -l "\EFI\debian\grubx64.efi"
#efibootmgr -c -d /dev/vdb -p 1 -L "Debian2" -l "\EFI\debian\grubx64.efi"


# NOW OUTSIDE THE chroot
sudo genfstab -U "$CHROOT_PATH" | sudo tee "$CHROOT_PATH/etc/fstab"
