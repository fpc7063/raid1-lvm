#!/bin/bash

# GENERAL CONFIG
export DEBIAN_FRONTEND=noninteractive

# SSH CONFIG
export REMOTE_USER=root
export REMOTE_HOST=192.168.122.147
export REMOTE_PASSWORD=""
export RS1=/home/fpc/teste/lvm-raid0-os/script.sh
export RS2=/home/fpc/teste/lvm-raid0-os/inside-chroot.sh

# LVM CONFIG
# VM TESTING DISKS
export DISKS="/dev/vda /dev/vdb"
# TRUE DISKS
#DISKS="/dev/nvme0n1p3 /dev/nvme1n1p3"
export EFI_SIZE=1G
export BOOT_SIZE=5G
export OS_LV_SIZE=13G
export VG_NAME="os-vg"
export LV_NAME="os-lv"
export ROOT_PASSWORD="password"

# CHROOT CONFIG
export CHROOT_PATH="/mnt/chroot"

# NEW USER
export USER_NAME="fpc"
export USER_ID=1000
export USER_COMMENT="well..."
export USER_SHELL="/bin/bash"
export USER_PASSWORD="testing"
