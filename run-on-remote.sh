#!/bin/bash

source vars.sh

set -ex

#sshpass -p "${REMOTE_PASSWORD}"
#SSH_OPTIONS="-o PreferredAuthentications=password"

ssh $SSH_OPTIONS "${REMOTE_USER}@${REMOTE_HOST}" 'mkdir -p /tmp/chroot/usr/bin'
scp $SSH_OPTIONS ./vars.sh "${REMOTE_USER}@${REMOTE_HOST}:/tmp/chroot/usr/bin"
scp $SSH_OPTIONS inside-chroot.sh "${REMOTE_USER}@${REMOTE_HOST}:/tmp/chroot/usr/bin"
ssh $SSH_OPTIONS "${REMOTE_USER}@${REMOTE_HOST}" 'chmod u+x /tmp/chroot/usr/bin/*'
ssh $SSH_OPTIONS "${REMOTE_USER}@${REMOTE_HOST}" 'bash -s' < "$RS1"
