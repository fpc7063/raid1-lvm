#!/bin/bash

set -x

source /usr/bin/vars.sh

apt-get install -y locales
sed -i -e 's/# en_US.UTF-8 UTF-8/en_US.UTF-8 UTF-8/' /etc/locale.gen && \
    echo 'LANG="en_US.UTF-8 UTF-8"'>/etc/default/locale
dpkg-reconfigure --frontend=noninteractive locales && \
	update-locale LANG=en_US.UTF-8 UTF-8


cat << EOF > /etc/apt/sources.list
deb http://deb.debian.org/debian/ bookworm main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian/ bookworm main contrib non-free-firmware non-free

deb http://security.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware
deb-src http://security.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware
EOF

apt update
apt upgrade -y

apt install -y firmware-linux linux-image-amd64 linux-headers-amd64 ovmf  lvm2 xfsprogs vim \
    grub-efi-amd64-signed grub-efi-amd64 grub-efi-amd64-bin grub-common grub2-common openssh-server sudo firmware-realtek \
    apparmor apt-utils base-passwd base-files binutils bash busybox bzip2 gzip xz-utils \
    ca-certificates console-setup cron cron-daemon-common curl wget dash dbus debianutils dkms \
    dnsmasq-base dns-root-data e2fsprogs exfatprogs firmware-amd-graphics gnupg2 gpg gvfs kmod \
    less man lsof p7zip p7zip-full systemd zstd dosfstools gparted

echo "root:$ROOT_PASSWORD" | chpasswd

#TODO: do I need this
#GRUB_OPTIONS="--target=x86_64-efi"
grub-install $GRUB_OPTIONS /dev/vda
grub-install $GRUB_OPTIONS /dev/vdb


update-grub
update-initramfs -u
#TODO: do I really need this?
update-grub


useradd "$USER_NAME" \
		--uid "$USER_ID" \
		--user-group \
		--comment "$USER_COMMENT" \
		--shell "$USER_SHELL" \
		--home "/home/$USER_NAME"
	HOME_DIR="/home/$USER_NAME"
	mkdir "$HOME_DIR"
	chown -R "$USER_NAME:$USER_NAME" "$HOME_DIR"
	echo "$USER_NAME:$USER_PASSWORD" | chpasswd
    adduser "$USER_NAME" sudo
